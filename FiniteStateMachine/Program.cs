﻿using System;

// Really good modern post about state machines
//https://deniskyashif.com/a-practical-guide-to-state-machines/

namespace FiniteStateMachine
{
    class Program
    {
        enum State
        {
            Open,
            Closed,
            Locked
        }

        enum Input
        {
            Open,
            Close,
            Lock,
            Unlock
        }

        static State ChangeState(State current, Input input) =>
            (current, input) switch
            {
                (State.Closed, Input.Open) => State.Open,
                (State.Open, Input.Close) => State.Closed,
                (State.Closed, Input.Lock) => State.Locked,
                (State.Locked, Input.Unlock) => State.Closed,
                _ => throw new NotSupportedException(
                    $"{current} has no transition on {input}")
            };

        static void Main(string[] args)
        {
            var current = State.Closed; // the initial state
            Console.WriteLine(current.ToString());
            current = ChangeState(current, Input.Open); // Opened
            Console.WriteLine(current.ToString());
            current = ChangeState(current, Input.Close); // Closed
            Console.WriteLine(current.ToString());
            current = ChangeState(current, Input.Lock); // Locked
            Console.WriteLine(current.ToString());
            current = ChangeState(current, Input.Open); // throws
        }
    }
}